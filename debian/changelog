django-classy-tags (4.1.0-1) unstable; urgency=medium

  * New upstream version 4.1.0
  * Bump Standards-Version to 4.6.2.
  * Build using pybuild-plugin-pyproject.
  * Switch to python3-pytest and python3-pytest-django to run tests.

 -- Michael Fladischer <fladi@debian.org>  Fri, 04 Aug 2023 17:57:00 +0000

django-classy-tags (4.0.0-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 4.6.1.0.
  * Update d/copyright with new years.
  * Refresh patches.

 -- Michael Fladischer <fladi@debian.org>  Wed, 04 Jan 2023 22:56:25 +0000

django-classy-tags (3.0.1-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + python-django-classy-tags-doc: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 14 Oct 2022 12:42:17 +0100

django-classy-tags (3.0.1-1) unstable; urgency=low

  * New upstream release.
  * Update patch to use local objects.inv in intersphinx mapping.

 -- Michael Fladischer <fladi@debian.org>  Tue, 15 Mar 2022 14:23:42 +0000

django-classy-tags (3.0.0-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches.
  * Depend on python3-all for autopkgtests.
  * Update year in d/copyright.

 -- Michael Fladischer <fladi@debian.org>  Wed, 26 Jan 2022 15:32:29 +0000

django-classy-tags (2.0.0-2) unstable; urgency=low

  * Update d/watch to work with github again.
  * Bump Standards-Version to 4.6.0.1.

 -- Michael Fladischer <fladi@debian.org>  Thu, 28 Oct 2021 15:36:41 +0000

django-classy-tags (2.0.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Michael Fladischer ]
  * New upstream release.
  * Update d/watch to work with github again.
  * Refresh patches.
  * Bump Standards-Version to 4.6.0.
  * Bump debhelper version to 13.
  * Use uscan version 4.
  * Add d/upstream/metadata.
  * Enable upstream testsuite for autopkgtests.
  * Remove unnecessary autopkgtest-pkg-python testsuite.

 -- Michael Fladischer <fladi@debian.org>  Mon, 30 Aug 2021 21:33:30 +0000

django-classy-tags (1.0.0-2) unstable; urgency=low

  * Tell autodep8 to use the correct import name (Closes: #948746).
  * Bump Standards-Version to 4.5.0.

 -- Michael Fladischer <fladi@debian.org>  Sun, 16 Feb 2020 11:13:01 +0100

django-classy-tags (1.0.0-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches.
  * Remove lintian override for missing GPG signature check on source
    tarball to serve as a reminder to improve on this.

 -- Michael Fladischer <fladi@debian.org>  Thu, 23 Jan 2020 14:20:20 +0100

django-classy-tags (0.9.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Michael Fladischer ]
  * New upstream release.
  * Add debian/gbp.conf.
  * Fix d/watch to use new upstream repo.
  * Refresh patches.
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Set Rules-Requires-Root: no.
  * Enable autopkgtest-pkg-python testsuite.

 -- Michael Fladischer <fladi@debian.org>  Mon, 30 Dec 2019 10:43:58 +0100

django-classy-tags (0.8.0-2) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * Convert git repository from git-dpm to gbp layout
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0 (no changes).

 -- Ondřej Nový <onovy@debian.org>  Fri, 09 Aug 2019 07:59:54 +0200

django-classy-tags (0.8.0-1) unstable; urgency=low

  * New upstream release.
  * Remove 0002-django-1.10-define_TEMPLATES.patch, fixed by upstream.
  * Use https:// for copyright-format 1.0 URL.

 -- Michael Fladischer <fladi@debian.org>  Thu, 01 Sep 2016 09:38:36 +0200

django-classy-tags (0.7.2-3) unstable; urgency=medium

  * Added myself as uploader.
  * Removed obsolete python version restriction X-Python{3,}-Version:.
  * Added patch to fix Django 1.10 FTBFS (Closes: #828644):
    - 0002-django-1.10-define_TEMPLATES.patch

 -- Thomas Goirand <zigo@debian.org>  Tue, 02 Aug 2016 10:08:09 +0000

django-classy-tags (0.7.2-2) unstable; urgency=low

  [ Ondřej Nový ]
  * Fixed VCS URL

  [ Michael Fladischer ]
  * Bump Standards-Version to 3.9.8.
  * Switch to python3-sphinx and drop versioned Build-Depends.

 -- Michael Fladischer <fladi@debian.org>  Fri, 20 May 2016 11:39:39 +0200

django-classy-tags (0.7.2-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.7.

 -- Michael Fladischer <fladi@debian.org>  Wed, 02 Mar 2016 18:27:48 +0100

django-classy-tags (0.7.1-1) unstable; urgency=low

  * New upstream release.
  * Use https in Vcs-Git field.

 -- Michael Fladischer <fladi@debian.org>  Fri, 29 Jan 2016 13:18:00 +0100

django-classy-tags (0.7.0-1) unstable; urgency=low

  [ Michael Fladischer ]
  * New upstream release.

  [ SVN-Git Migration ]
  * git-dpm config
  * Update Vcs fields for git migration

 -- Michael Fladischer <fladi@debian.org>  Sat, 19 Dec 2015 21:20:36 +0100

django-classy-tags (0.6.2-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Sun, 14 Jun 2015 20:15:49 +0200

django-classy-tags (0.6.0-1) unstable; urgency=medium

  * New upstream release.
  * Drop django-1.7.patch as compatibility was fixed by upstream.
  * Bump Standards-Version to 3.9.6.
  * Install upstream changelog and drop lintian-overrides accordingly.
  * Change my email address to fladi@debian.org.

 -- Michael Fladischer <fladi@debian.org>  Mon, 18 May 2015 14:07:08 +0200

django-classy-tags (0.5.1-4) unstable; urgency=low

  * Switch buildsystem to pybuild.
  * Add dh-python to Build-Depends.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Thu, 28 Aug 2014 21:51:07 +0200

django-classy-tags (0.5.1-3) unstable; urgency=medium

  * Add django-1.7.patch to make tests run with Django-1.7 (Closes: #755595).
  * Add Python3 support.
  * Fix grammar mistakes in package description (Closes: #688340).

 -- Michael Fladischer <FladischerMichael@fladi.at>  Thu, 21 Aug 2014 14:21:52 +0200

django-classy-tags (0.5.1-2) unstable; urgency=low

  * Change architecture to "all" as there are no architecture dependent parts
    in the binary package.
  * Add intersphinx.patch to disable objects.inv downloads for shpinx.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Fri, 25 Apr 2014 10:40:32 +0200

django-classy-tags (0.5.1-1) unstable; urgency=low

  * New upstream release.
  * Take over maintainership for DPMT and add myself as uploader, Thanks to
    Ondřej Surý for his work on this package.
  * Switch to dh_python2.
  * Change VCS-* fields to reflect moving the repository to subversion.
  * Remove debian/gbp.conf as DPMT uses subversion.
  * Bump Standards-Version to 3.9.5.
  * Bump debhelper version to 9.
  * Bump minimum Python version to 2.6.
  * Build documentation using sphinx and ship it in a spearate package.
  * Add ${python:Depends} to python-django-classy-tags binary package
    (Closes: #676401).
  * Make Depends on python-django versioned with (> 1.5).
  * Convert debian/copyright to copyright format 1.0.
  * Add myself in debian/copyright.
  * Run tests during build.
    + Add python-django (>= 1.5) to Build-Depends.
  * Clean up files in django_classy_tags.egg-info/ to allow two or more builds
    in a row.
  * Add debian/watch file.
  * Add lintian override for missing PGP signature on upstream tarball.
  * Add lintian overrides for missing upstream changelog.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Thu, 17 Apr 2014 12:53:16 +0200

django-classy-tags (0.3.4.1-1) unstable; urgency=low

  * Initial release (Closes: #660672)

 -- Ondřej Surý <ondrej@debian.org>  Mon, 20 Feb 2012 21:25:21 +0100
